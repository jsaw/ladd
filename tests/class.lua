#!/usr/bin/env delua-5.3

unpack = table.unpack -- Eclipse Lua debugger compat (post 5.2) 

require 'ladd.class'

--[[ declaration ]]
class 'my.A' {
    a = 1,
}

function my.A:__init()
    print("my.A:__init()")
end

function my.A:__gc()
    print("my.A:__gc()")
end

assert(isclass(my.A))
assert(not isobject(my.A))
assert(toclass(my.A) == my.A)
assert(toclass('my.A') == my.A)
assert(not isa(my.A, my.A))

class 'my.B' {
    __super__ = { my.A },
    a = 2,
    b = void,
}

function my.B:__init()
    print("my.B:__init()")
    self:__parent__()
end

function my.B:__gc()
    print("my.B:__gc()")
end

assert(isclass(my.B))
assert(not isobject(my.B))
assert(toclass(my.B) == my.B)
assert(toclass('my.B') == my.B)

--[[ object ]]
local A = my.A()
assert(isobject(A))
assert(isa(A, my.A))
assert(isa(A, "my.A"))
assert(A:isa(my.A))
assert(A:isa{my.A})
assert(A.a == 1)

local B = my.B()
assert(isobject(B))
assert(B:isa(my.B))
assert(B:isa{my.A})
assert(B.a == 2)
assert(isvoid(B.b))
assert(isvoidornil(B.b))
assert(not isnil(B.b))

