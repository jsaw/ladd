--[[
Copyright (C) 2016-2019 Research center caesar, Bonn, Germany
Author(s): 
    Jürgen "George" Sawinski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local tinsert = table.insert
local strfind = string.find
local strsub = string.sub
local lua_tostring = tostring
local strsplit = function(text, delimiter)
    -- take from http://lua-users.org/wiki/SplitJoin
    local list = {}
    local pos = 1
--    if strfind("", delimiter, 1) then -- this would result in endless loops
--        error("delimiter matches empty string!")
--    end
    while 1 do
        local first, last = strfind(text, delimiter, pos)
        if first then -- found?
            tinsert(list, strsub(text, pos, first-1))
            pos = last+1
        else
            tinsert(list, strsub(text, pos))
            break
        end
    end
    return list
end

--[[- 
    @module ladd.class Class implementation.
    
    This represents one of many ways to implement classes in Lua.
    It supports a simple inheritance scheme, as well as singletons.
    
    Conventions:<br/>
    As Lua, without major intervention, does not have a concept of
    public (or visible) or private (or hidden) table contents, the
    following member naming conventions should be adhered:
    
    - public (or visible) members do start with a letter
    - private (or hidden) members (as well as [metamethods](https://www.lua.org/manual/5.3/manual.html#2.4))
      start with double underscore (__)
    
    Class declaration (class system implementation) specific variables or tags are prefixed
    and suffixed with a double underscore (e.g. ``__class__``).
    
    @usage 'Motivational example'
    @code.lua
        class 'io.stream' {
            __super__ = { io.streambase },
            __init = function(self, ...)
                    io.streambase.__init(self)
                    
                    if #{...} > 0 then
                        self:open(...)
                    end
                end,
            __gc = function(self)
                    self:close()
                end,
        }
        
        function io.stream:open(...)
            -- ... 
        end
        
        function io.stream:close()
            -- ... 
        end
    @endcode
]]
local M = require 'ladd.module'

--- @internal Reserved keywords.
local __reserved__ = {
    __parent__ = true,
    __super__ = false,
    __member__ = false
}

--- @internal Class metatable.
local __meta__ = {}

--- Check if @a value is a class declaration.
-- 
local function isclass(value)
    return getmetatable(value) == __meta__
end

--- Check if @a value is an object.
-- 
local function isobject(value)
    return getmetatable(getmetatable(value)) == __meta__
end

--- Get class by name or object.
-- 
-- FIXME
--
local function toclass(value)
    local mt = getmetatable(value)
    if mt == __meta__ then -- class declaration
        return value
    elseif type(value) == 'string' then -- class name
        local res = _G
        for _,k in ipairs(strsplit(value, '[.]')) do
            res = res[k]
            
            if not res then
                return nil
            elseif type(res) ~= 'table' then
                error("invalid namespace")
            end
        end
        
        assert(isclass(res))
        return res
    elseif getmetatable(mt) == __meta__ then -- object
        return mt
    else
        error("not a class or object")
    end
end

--- A void value.
-- 
-- In Lua, assigning a ``nil`` to a table key erases the table key.
-- Thus, to be able to differentiate between an unassigned object
-- member and a nil value, the ``void`` type is introduced.
-- 
local void = {}

--- @addto void
-- 
-- In addition, it provides the ``__call`` method returning ``void``
-- itself, thus, providing a function call stub (e.g. for the __parent__
-- in a class declaration).
-- 
-- @see class
-- 
setmetatable(void, {
    __call = function() 
            return void
        end
})

--- Check if a @a value is ``void``.
-- 
local function isvoid(value)
    return value == void
end

--- Check if a @a value is ``nil``. 
-- 
local function isnil(value)
    return value == nil
end

--- Check if a variable is ``void`` or ``nil``.
-- 
local function isvoidornil(value)
    return (value == nil) or (value == void)
end

--- Check if object is of a specific or inherited type or class.
-- @param object The object to test.
-- @param declaration A \<class\> or { \<(super-)class\> } declaration.
-- 
-- This function (also available as object method) checks if the 
-- @a object is of type @a declaration or an inherited class if enclosed as
-- a table. 
-- 
-- @return @c true if class or super class matches, @c false otherwise.
local function isa(object, declaration)
    -- utility to match super class
    function isderived(cls, super)
        for _,c in ipairs(cls.__super__) do
            if c == super then
                return true
            else
                if isderived(c.__super__) then
                    return true
                end
            end
        end
    end

    -- parse arguments
    local check_super = false
    if type(declaration) == 'table' and not getmetatable(declaration) then
        assert(#declaration == 1)
        
        check_super = true
        declaration = declaration[1]
    end

    -- object meta-table
    local mt = getmetatable(object)

    -- check
    if getmetatable(mt) == __meta__ then -- object
        declaration = toclass(declaration)
        
        return (mt == declaration)
            or (check_super and isderived(mt, declaration))
    else -- Lua type
        return (mt == declaration) 
            or (type(object) == declaration)
    end
end

--- Default copy constructor (creating a deep copy).
local function __copy(self) 
    local mt = getmetatable(self)
    
    if mt == __meta__ or self == void then
        -- class declarations are returned as is
        return self
    elseif getmetatable(mt) == __meta__ then
        -- objects are returned as is unless they have their own __copy method
        if mt.__copy ~= __copy then
            return mt.__copy(self)
        else
            return self
        end
    elseif type(self) == 'table' then
        -- deep table copy
        local res = {}
        
        for k,v in pairs(self) do
            res[k] = __copy(v)
        end
        
        return res
    else
        return self
    end
end

--- Class declaration.
-- 
-- @syntax
--     class '<classname>' {
--         <class-declaration>
--     }
-- 
-- FIXME
-- 
-- @usage "Declaring a class."
-- class 'mynamespace.myclass' {
--    -- inheritance
--    __super__ = { otherclass },
--    
--    -- variables
--    a_variable = null,
-- }
-- 
-- function mynamespace.myclass:__init(...)
--     -- constructor code
-- end
-- 
-- function mynamespace.myclass:__gc()
--     -- destructor code
-- end
-- 
-- @usage "Declaring a singleton."
-- local singleton_instance
-- 
-- class 'singleton' {}
-- 
-- function singleton:__init()
--     if singleton_instance then
--         return singleton_instance
--     end
--     
--     singleton_instance = self
-- end
-- 
-- @note This class system implementation relies on the command-line-like 
--       [function call](https://www.lua.org/manual/5.3/manual.html#3.4.10)
--       conventions, where the enclosing parentheses of arguments can be 
--       omitted for a single literal string or table. 
--  
function class(name)
    -- local class declaration? 
    local islocal = (name:sub(1,1) == '@')

    -- declaration generator function
    return function(T)
            assert(type(T) == 'table')

            -- class skeleton     
            local declaration = {
                __parent__ = void,  -- "direct" parent
                __super__ = {},         -- other parents (super classes)
                __member__ = {},        -- object member variables
            }
            declaration.__index = declaration
            setmetatable(declaration, __meta__)
            
            -- handle inheritance (bottom to top, right to left)
            local function inherit(classdecl)
                assert(isclass(classdecl))

                -- recurse
                for k = #classdecl.__super__,1,-1 do
                    local super = classdecl.__super__[k]                    
                    inherit(super)
                end
                    
                -- members
                for i,m in pairs(classdecl.__member__) do
                    declaration.__member__[i] = m
                end
                
                -- methods
                for i,m in pairs(classdecl) do
                    if type(m) == 'function' then
                        declaration[i] = m
                    end
                end
            end

            for _,super in ipairs(T.__super__ or {}) do
                inherit(super)
                tinsert(declaration.__super__, super)
                
                if declaration.__parent__ == void then
                    declaration.__parent__ = super.__init or void
                end
            end
            
            -- declaration members and methods
            for k,m in pairs(T) do
                if __reserved__[k] ~= nil then
                    if __reserved__[k] then
                        error(k.." is a reserved keyword.")
                    end
                elseif type(m) == 'function' then
                    -- method
                    declaration[k] = m
                else
                    -- member
                    declaration.__member__[k] = m
                end
            end
            
            -- namespace and classname
            if islocal then
                declaration.__class__ = name
            else
                local _,_,classname = name:find('[.]?([^.]*)$')
                local _,_,namespacename = name:find('^(.*)[.][^.]*$')
                local namespace = _G            

                local acc_ns_name = nil -- accumulated namespace name (for error messages)
                for k in name:gmatch('([^.]*)[.]') do
                    local T = type(namespace[k])
                    
                    -- accumulated namespace path (for error message)
                    if acc_ns_name then
                        acc_ns_name = acc_ns_name .. '.' .. k
                    else
                        acc_ns_name = k
                    end

                    -- create global table if necessary                    
                    if T == 'nil' then
                        namespace[k] = {}
                    elseif T ~= 'table' then
                        error(string.format('ladd.namespace: expected a table (or nil) at %q', acc_ns_name))
                    end
                    
                    -- next namespace table
                    namespace = namespace[k]
                end
                
                declaration.__class__ = classname
                namespace[classname] = declaration
            end
            
            -- object methods
            declaration.__copy = __copy
            declaration.isa = isa
            declaration.tostring = function(self)
                    return self.__class__
                end
                
            return declaration
        end
end

--- @internal Class (constructor) call
function __meta__.__call(cls, ...)
    local obj = __copy(cls.__member__)
    setmetatable(obj, cls)
    
    if cls.__init then
        obj = cls.__init(obj, ...) or obj -- this magic allows singletons!
    end
    
    return obj
end

--- Convert to string.
-- 
-- FIXME
-- 
local function tostring(obj)
    if isclass(obj) then
        return obj.__class__
    elseif isobject(obj) then
        if obj.tostring then
            return obj:tostring()
        end
    end
    
    return lua_tostring(obj)
end

--[[ GLOBAL ]]
_G.isclass = isclass
_G.toclass = toclass
_G.isobject = isobject
_G.void = void
_G.isvoid = isvoid
_G.isvoidornil = isvoidornil
_G.isnil = isnil
_G.isa = isa
_G.class = class
_G.tostring = tostring
        
--[[ MODULE ]]
M.__reserved__ = __reserved__
M.isclass = isclass
M.toclass = toclass
M.isobject = isobject
M.void = void
M.isvoid = isvoid
M.isvoidornil = isvoidornil
M.isnil = isnil
M.isa = isa
M.class = class
M.__copy = __copy
M.tostring = tostring

return M
