--[[
Copyright (C) 2018-2019 Research center caesar, Bonn, Germany
Author(s): 
    Jürgen "George" Sawinski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local M = require 'ladd.io.pipe'

--- Stream filter.
-- 
-- FIXME
-- 
-- @class M.filter
M.filter = class '@ladd.io.filter' {
    __super__ = { M.pipe },
    
    __filterfn = function(m) return m; end,
}

--- Set filter function.
-- 
function M.filter:set(fn)
    self.__filterfn = fn
end

function M.filter:read(...)
    return self.__filterfn(self.__source:read(...))
end

function M.filter:__pairs()
    local function stateless_iter(obj)
        return obj:read("L")
    end

    return stateless_iter, self
end

--[[ MODULE ]]
return M
