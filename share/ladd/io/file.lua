--[[
Copyright (C) 2018-2019 Research center caesar, Bonn, Germany
Author(s): 
    Jürgen "George" Sawinski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local M = require 'ladd.io.module'

--- File stream.
-- 
-- @see ladd.io.stream
-- @class ladd.io.file
M.file = class '@ladd.io.file' {
    __super__ = { M.stream },
    __f = null,
}

--- Constructor.
function M.file:__init(filename, mode)
    self.__f = io.open(filename, mode)
end

function M.file:setvbuf(mode, size)
    return self.__f:setvbuf(mode, size)
end

function M.file:seek(whence, offset)
    return self.__f:seek(whence, offset)
end

function M.file:write(...)
    return self.__f:write(...)
end

function M.file:read(...)
    return self.__f:read(...)
end

function M.file:flush()
    return self.__f:flush()
end

function M.file:lines(...)
    return self.__f:lines(...)
end

function M.file:close()
    return self.__f:close()
end

--- Standard streams.
-- 
-- @see ladd.io.stdin, ladd.io.stdout, ladd.io.stderr
-- @local
M.stdstream = class '@ladd.io.stdstream' {
    __super__ = { M.file },
    __f = null,
}

--- Constructor.
--
-- @class ladd.io.stdstream
-- @local
function M.stdstream:__init(stdstream)
    self.__f = stdstream
end

--- Standard IO namespace.
M.std = {}

--- Standard input stream.
M.std.input = M.stdstream(io.stdin)
--- Standard output stream.
M.std.output = M.stdstream(io.stdout)
--- Standard error stream.
M.std.error = M.stdstream(io.stderr)

--[[ MODULE ]]
return M
