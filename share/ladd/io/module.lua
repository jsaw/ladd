--[[
Copyright (C) 2018-2019 Research center caesar, Bonn, Germany
Author(s): 
    Jürgen "George" Sawinski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

require 'ladd.class'

local strfmt = string.format

-- locals
local tunpack = table.unpack

--[[- 
    @module ladd.io IO operations.
    
    The IO operations are modeled after C++ operations with a bit more 
    of a touch of Lua.    
    
]]
local M = io

--- Base class for IO operations.
-- 
-- The empty base class defines the interface for in-/output operations.
-- 
-- @see ladd.io.file
-- @see ladd.io.stdin, ladd.io.stdout, ladd.io.stderr
-- @class ladd.io.stream
M.stream = class '@ladd.io.stream' {}

--- Constructor.
function M.stream:__init(...)
end

--- Destructor.
-- 
-- Close stream and cleanup associated memory.
function M.stream:__gc()
    -- close stream if it is not closed yet
    pcall(self.close, self)
end

--- Set buffer mode.
-- 
-- Sets the buffering mode for an output file. There are three available modes:
-- 
--      @b "no": no buffering; the result of any output operation appears immediately.
--      @b "full": full buffering; output operation is performed only when the buffer is full or when you explicitly flush the file (see io.flush).
--      @b "line": line buffering; output is buffered until a newline is output or there is any input from some special files (such as a terminal device).
-- 
-- For the last two cases, size specifies the size of the buffer, in bytes. The default is an 
-- appropriate size.
function M.stream:setvbuf(mode, size)
    error("setvbuf not supported by stream")
end

--- Seek file position.
-- 
-- Sets and gets the file position, measured from the beginning of the file, to the position 
-- given by offset plus a base specified by the string whence, as follows:
-- 
--    "set": base is position 0 (beginning of the file);
--    "cur": base is current position;
--    "end": base is end of file;
--
-- In case of success, seek returns the final file position, measured in bytes from the 
-- beginning of the file. If seek fails, it returns nil, plus a string describing the error.
--
-- The default value for whence is "cur", and for offset is 0. Therefore, the call file:seek() 
-- returns the current file position, without changing it; the call file:seek("set") sets the position 
-- to the beginning of the file (and returns 0); and the call file:seek("end") sets the position to 
-- the end of the file, and returns its size. 
function M.stream:seek(whence, offset)
    error("seek not supported by stream")
end

--- Write to stream.
-- 
--  Writes the value of each of its arguments to file. The arguments must be strings or numbers.
--
-- In case of success, this function returns file. Otherwise it returns nil plus a string describing the error.
function M.stream:write(...)
    error("write not supported by stream")
end

--- Write formatted to stream.
function M.stream:format(...)
    self:write(strfmt(...))
end

--- Read from stream.
-- 
--  Reads the file file, according to the given formats, which specify what to read. 
--  For each format, the function returns a string or a number with the characters read, 
--  or nil if it cannot read data with the specified format. (In this latter case, the 
--  function does not read subsequent formats.) When called without formats, it uses a 
--  default format that reads the next line (see below).
--
-- The available formats are
--
--     "n": reads a numeral and returns it as a float or an integer, following the lexical 
--          conventions of Lua. (The numeral may have leading spaces and a sign.) This format 
--          always reads the longest input sequence that is a valid prefix for a numeral; if 
--          that prefix does not form a valid numeral (e.g., an empty string, "0x", or "3.4e-"),
--          it is discarded and the function returns nil.
--     "a": reads the whole file, starting at the current position. On end of file, it returns the empty string.
--     "l": reads the next line skipping the end of line, returning nil on end of file. This is the default format.
--     "L": reads the next line keeping the end-of-line character (if present), returning nil on end of file.
--     number: reads a string with up to this number of bytes, returning nil on end of file. If number is zero, it reads nothing and returns an empty string, or nil on end of file.
-- 
-- The formats "l" and "L" should be used only for text files. 
function M.stream:read(...)
    error("read not supported by stream")
end

--- Flush the stream.
function M.stream:flush()
    error("flush not supported by stream")
end

--- Get line iterator.
-- 
--  Returns an iterator function that, each time it is called, reads the file according 
--  to the given formats. When no format is given, uses "l" as a default. As an example, 
--  the construction
--
--       for c in file:lines(1) do body end
--
-- will iterate over all characters of the file, starting at the current position. Unlike 
-- io.lines, this function does not close the file when the loop ends.
--
-- In case of errors this function raises the error, instead of returning an error code. 
function M.stream:lines(...)
    local args = {...}
    return function()
        self:read(tunpack(args)) 
    end
end

--- Close the stream.
-- 
-- @see M.stream:__gc()
function M.stream:close()
    error("close not supported by stream")
end

--- Get stream iterator (__pairs meta-method).
-- 
-- Returns an iterator function, that each time it is called, reads an item (e.g. a token)
-- from the stream. The value returned by the iterator is implementation specific, however,
-- the default implementation returns the default line iterator.
-- 
-- @see ladd.io.filter
function M.stream:__pairs()
    return self:lines('*L')
end

--[[ MODULE ]]
return M
