--[[
Copyright (C) 2018-2019 Research center caesar, Bonn, Germany
Author(s): 
    Jürgen "George" Sawinski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local M = require 'ladd.io.module'

local strfmt = string.format

--- Pipe.
--
-- A pipe represents a stream with a source and a sink.
-- 
-- @usage
-- local io = M.pipe(M.stdin, M.stdout)
-- for l in pairs(io) do
--     io:format("%s\n", l)
-- end
-- 
M.pipe = class '@ladd.io.pipe' {
    __source = null,
    __sink = null,
}

--- Constructor.
-- 
-- FIXME
-- 
function M.pipe:__init(source, sink)
    if not isa(source, {M.stream}) then
        error("Expected a stream as source.")
    end
    
    self.__source = source
    self.__sink = sink
end

--- Destructor.
function M.stream:__gc()
    pcall(self.close, self)
end

--- Close source and sink.
-- 
function M.pipe:close()
    if not isnullornil(self.__source) then
        self.__source:close()
    end
    if not isnullornil(self.__sink) then
        self.__sink:close()
    end
end

--- Write to sink.
-- 
-- @see M.stream:write(...)
function M.pipe:write(...)
    return self.__sink:write(...)
end

--- Write formatted to pipe.
-- 
-- @see M.pipe:write(...)
function M.pipe:format(...)
    self:write(strfmt(...))
end

--- Read from source
-- 
-- @see M.stream:read(...)
function M.pipe:read(...)
    return self.__source:read(...)
end

--- Get line iterator of the source.
-- 
-- @see M.stream:lines(...)
function M.pipe:lines(...)
    return self.__source:lines(...)
end

--- Flush pipe.
-- 
-- FIXME
-- 
-- @see M.filter
function M.pipe:flush()
    for item in pairs(self) do
        self.__sink:write(item)
    end
end

--- Get stream iterator (__pairs meta-method).
-- 
-- @see M.stream:__pairs()
function M.pipe:__pairs()
    return self.__source:__pairs()
end

--[[ MODULE ]]
return M
