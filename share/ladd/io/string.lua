--[[
Copyright (C) 2018-2019 Research center caesar, Bonn, Germany
Author(s): 
    Jürgen "George" Sawinski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local M = require 'ladd.io.module'

local strfmt = string.format

--- String stream
-- 
-- This sports a very naive string stream implementation. It works basically
-- simlar to a file opened in read-write mode.
-- 
-- A single addition to the standard interface is made, the @c tostring method.
-- 
-- @class ladd.io.string
M.string = class '@ladd.io.string' {
    __super__ = { M.stream },
    __strbuf  = '',
    __strpos  = 1,
}

--- Constructor.
function M.string:__init(str, mode)
    self.__strbuf = str or ""
    self.__strpos = 1
    
    if mode then
        error("Internal error: mode not (yet) supported.")
    end
end

function M.string:seek(whence, offset)
    whence = whence or 'cur'
    offset = offset or 0

    -- calculate new position
    local curpos
    if whence == 'set' then
        if offset < 0 then
            return nil, 'Invalid argument'
        end
        curpos = 0
    elseif whence == 'cur' then
        curpos = self.__strpos 
    elseif whence == 'end' then
        curpos = #self.__strbuf
    else
        error(strfmt("bad argument #1 to 'seek' (invalid option %q)", tostring(whence)))    
    end
    
    local newpos = curpos + offset
    
    -- check 
    if newpos < 0 then
        return nil, 'Invalid argument'
    elseif newpos > #self.__strbuf then
        -- FIXME what to do, auto-expand string?
        return nil, 'Invalid argument'
    end
    
    -- assign and return
    self.__strpos = newpos
    return self.__strpos
end

function M.string:write(...)
    local arg = {...}
    
    local head,tail = self.__strbuf:sub(1,self.__strpos), self.__strbuf:sub(self.__strpos+1,-1)
    
    local str = ''
    for _,s in ipairs(arg) do
        str = str..s
    end
    
    self.__strbuf = head..str..tail
    self.__strpos = self.__strpos + #str
end

function M.string:read(...)
    -- get buffer part (and adjust __strpos)
    local function sub(from, to, skip)
        if from > #self.__strbuf then
            return nil
        end
        
        self.__strpos = to + (skip or 0) + 1
        
        return self.__strbuf:sub(from, to)
    end

    -- format utility
    local function scan(what)
        if type(what) == 'string' then
            local wh = what:sub(1,2)
    
            if wh == '*l' then
                local pos,nxt = self.__strbuf:find('[\r][\n])', self.__strpos)
                if not pos then
                    pos,nxt = self.__strbuf:find('[\r\n]', self.__strpos)
                end

                if pos then
                    return sub(self.__strpos, pos-1, nxt-pos+1)
                else
                    return sub(self.__strpos, #self.__strbuf)
                end
            elseif wh == '*a' then
                return sub(self.__strpos, #self.__strbuf)
            end
        end
        
        error(strfmt("Internal error: %q format not (yet) supported.", tostring(what)))
    end

    local arg = {...}
    local res = {}
    
    if #arg == 0 then
        return scan('*l')
    else
        for i,a in ipairs(arg) do
            res[i] = scan(a)
        end
    end
    
    return tunpack(res)
end

function M.string:flush()
    -- ignore
end

function M.string:close()
    -- ignored
end

function M.string:tostring()
    return self.__strbuf
end

--[[ MODULE ]]
return M
