--[[
Copyright (C) 2018-2019 Research center caesar, Bonn, Germany
Author(s): 
    Jürgen "George" Sawinski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]


local M = require 'ladd.program._module_'

-- requires
require 'ladd.class'
local iox = require 'ladd.io'

-- locals
local tinsert = table.insert
local tunpack = table.unpack

local stdout = iox.std.output
local stderr = iox.std.error

local __meta__ = {}

--- Parse program command line options.
-- 
-- @param args Program args (or a previous options table).
-- @param decl Options table.
-- @return Result table with option keys and arguments.
-- 
-- The option table consist of argument parsing parameters:
-- @list
--     @c{unparsed = "<boolean>"} to return list of unparsed options,
--     @c{arguments = "<boolean>"} to return list of non-option arguments,
--     ...
--     
-- and option entries, a table with
-- @list
--     @c{'<short-option-letter>'},
--     [@c{'<long-option>'},]
--     [@c{name = '<option-name>'},]
--     [@c{parameter = <option-parameter (default: 'flag')>},]
--     [@c{errorcode = <program-exit-code-on-option-error>},]
--     [@c{default = <default-option-value (default: @c true)},]
--
-- 
function M.options(args, decl)
    local result = {}

    --[[ initialize ]]
    decl = decl or {}

    -- sort short/long opts
    local short = {}
    local long = {}
    for i,t in ipairs(decl) do
        for _,k in ipairs(t) do
            if #k == 1 then
                short[k] = i
            else
                long[k] = i
            end
        end
    end

    -- set 'name' and 'parameter' field
    for i,t in ipairs(decl) do
        -- name
        if not t.name then
            for _,k in ipairs(t) do
                if #k > 1 then
                    t.name = k
                end
            end
        end
        
        if not t.name then
            error("Error in option declaration: 'name' field missing.")
        end
        
        -- parameter
        if t.default ~= nil then
            local T = type(t.default)
            if T == 'boolean' then
                t.parameter = t.parameter or 'flag'
            else
                t.parameter = t.parameter or 'optional'
            end
        else
            t.parameter = t.parameter or 'flag'
        end
    end

    -- arguments/unparsed tables
    if decl.arguments then
        result.arguments = {}
    end
    
    if decl.unparsed then
        result.unparsed = {}
    end
    
    --[[ parse options ]]
    local N = #args
    local n = 1

    -- add option (value if passed with '=')
    local function addopt(idx, opt, value)
        local t = decl[idx]
        
        if t.parameter == 'flag' then
            if value then
                stderr:format("Invalid value passed to option: %s\n", opt)
                stderr:format("Use --help for further information.\n")
                os.exit(decl.errorcode or 1)
            end
        
            result[t.name] = t.default or true
        elseif t.parameter == 'optional' then
            result[t.name] = value or t.default or true
        elseif t.parameter == 'multiple' then
            result[t.name] = result[t.name] or {}
            
            if not value then
                n = n+1
                value = args[n]
            end
            
            tinsert(result[t.name], value)
        elseif t.parameter == 'count' then
            result[t.name] = (result[t.name] or 0) + 1
        elseif type(t.parameter) == 'number' 
                or (type(t.parameter) == 'boolean' and t.parameter) then
            if t.parameter == 1 then
                if result[t.name] then
                    stderr:format('Option %s already set.\n', opt)
                    stderr:format("Use --help for further information.\n")
                    os.exit(decl.errorcode or 1)        
                end
    
                if not value then
                    n = n+1
                    value = args[n]
                end
                
                result[t.name] = value
            else
                if result[t.name] and not type(result[t.name]) == 'table' then
                    result[t.name] = { result[t.name] }
                end
                result[t.name] = result[t.name] or {}

                if #result[t.name] > t.parameter then
                    stderr:format('Option %s takes a maxium of %d parameters.\n', opt, t.parameter)
                    stderr:format("Use --help for further information.\n")
                    os.exit(decl.errorcode or 1)        
                end
    
                tinsert(result[t.name], value)
            end
            -- FIXME
        elseif type(t.parameter) == 'boolean' then
            -- ignore
        else 
            error("Internal error.")
        end
        
        if n > N then
            stderr:format('Expected a parameter for option %s\n', opt)
            stderr:format("Use --help for further information.\n")
            os.exit(decl.errorcode or 1)        
        end
    end

    -- loop over args
    while n <= N do
        local opt = args[n]
        if opt:find('^%-%-') then
            -- parse option key-value pair
            local key = opt:match("^.-=")
            local value
            
            if key then
                key = key:sub(3, -2)
                value = opt:match("=.*$"):sub(2,-1)
            else
                key = opt:sub(3,-1)
            end
            
            -- check if option exists
            if not long[key] then
                if decl.unparsed then
                    tinsert(result.unparsed, opt)
                else
                    stderr:format("Invalid option: %s\n", opt)
                    stderr:format("Use --help for further information.\n")
                    os.exit(decl.errorcode or 1)
                end
            else
                addopt(long[key],opt,value)
            end
        elseif opt:find('^%-') then
            for i = 2,#opt do
                local key = opt:sub(i,i)
            
                if key == '=' then
                    stderr:format("Invalid character '=' in short option: %s\n", opt)
                    stderr:format("Use --help for further information.\n")
                    os.exit(decl.errorcode or 1)
                end
                            
                if not short[key] then
                    if decl.unparsed then
                        result.unparsed = result.unparsed or {}
                        tinsert(result.unparsed, '-'..key)
                    else
                        stderr:format("Invalid option: %s\n", opt)
                        stderr:format("Use --help for further information.\n")
                        os.exit(decl.errorcode or 1)
                    end
                else                
                    addopt(short[key], opt)
                end
            end
        else
            if decl.arguments then
                tinsert(result.arguments, opt) 
            elseif decl.unparsed then
                tinsert(result.unparsed, opt)
            else
                stderr:format("Invalid argument: %s\n", opt)
                stderr:format("Use --help for further information.\n")
                
                os.exit(decl.errorcode or 1)
            end
        end
        
        n = n + 1
    end
   
    return result
end

--[[ MODULE ]]
return M
