--[[
Copyright (C) 2018-2019 Research center caesar, Bonn, Germany
Author(s): 
    Jürgen "George" Sawinski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]


local M = require 'ladd.program._module_'

-- requires
require 'ladd.class'
local string = require 'ladd.string'
local strsplit = string.split

local tunpack = table.unpack

--- Version information.
--
-- FIXME
--
local version = class "@shared.program.version" {
    major = void,
    minor = void,
    patch = void,
    build = void,
}

function version:__init(...)
    local args = {...}
    
    if #args == 1 then
        assert(isa(args[1], 'string'))       
        self:__init(tunpack(strsplit(args[1], '[.]')))
    else
        assert(isa(args[1], 'number'))
        self.major = args[1]
        
        if args[2] then
            assert(isa(args[2], 'number'))
            self.minor = args[2]

            if args[3] then
                assert(isa(args[3], 'number'))
                self.patch = args[3]
    
                if args[4] then
                    assert(isa(args[4], 'number'))
                    self.build = args[4]
                end
            end
        end
    end
end

function version:__call(...)
    return version(...)
end

function version:__eq(other)
    local result = (self.major == other.major)
    
    if result and not isvoidornil(self.minor) and not isvoidornil(other.minor) then
        result = (self.minor == other.minor)
        if result and not isvoidornil(self.patch) and not isvoidornil(other.patch) then
            result = (self.patch == other.patch)
            if result and not isvoidornil(self.build) and not isvoidornil(other.build) then
                result = (self.build == other.build)
            end
        end
    end
    
    return result
end

function version:__lt(other)
    local result = (self.major < other.major)
    
    if (self.major == other.major) and not isvoidornil(self.minor) and not isvoidornil(other.minor) then
        result = (self.minor < other.minor)
        if (self.minor == other.minor) and not isvoidornil(self.patch) and not isvoidornil(other.patch) then
            result = (self.patch < other.patch)
            if (self.patch == other.patch) and not isvoidornil(self.build) and not isvoidornil(other.build) then
                result = (self.build < other.build)
            end
        end
    end
    
    return result
end

function version:__le(other)
    local result = (self.major <= other.major)
    
    if result and not isvoidornil(self.minor) and not isvoidornil(other.minor) then
        result = (self.minor <= other.minor)
        if result and not isvoidornil(self.patch) and not isvoidornil(other.patch) then
            result = (self.patch <= other.patch)
            if result and not isvoidornil(self.build) and not isvoidornil(other.build) then
                result = (self.build <= other.build)
            end
        end
    end
    
    return result
end

function version:tostring()
    local result = string.format('%d', self.major)
    
    if not isvoidornil(self.minor) then
        result = string.format('%s.%d', result, self.minor)
        if not isvoidornil(self.patch) then
            result = string.format('%s.%d', result, self.patch)
            if not isvoidornil(self.build) then
                result = string.format('%s.%d', result, self.build)
            end
        end
    end
    
    return result
end

--[[ MODULE ]]
M.version = version
return M
