--[[
Copyright (C) 2018-2019 Research center caesar, Bonn, Germany
Author(s): 
    Jürgen "George" Sawinski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local M = require 'ladd.program._module_'

require 'ladd.class'

local sys = require 'ladd.sys'
local fs = require 'ladd.fs'
local io = require 'ladd.io'
local table = require 'ladd.table'
local paths = require 'ladd.program.paths'
local string = require 'ladd.string'

local strsplit = string.split
local tdup = table.duplicate
local tinsert = table.insert

--- Settings (class).
-- 
-- FIXME
-- 
local settings = class '@ladd.program.settings' {
    filename = void,
    autosave = void,
    settings = void,
}

function settings:__init(p, default_settings)
    -- parameter defaults
    p = p or {}
    
    local function setp(key, value)
        if p[key] == nil then
            rawset(self, key, value)
        else
            rawset(self, key, p[key])
        end
    end

    setp('filename', fs.fullfile(M.settings_path(), 'config'))
    setp('autosave', true)
        
    -- load
    self:reset(default_settings or {})
    self:load()
    
    -- auto save
    if self.autosave then
        self:save()
    end
end

function settings:__gc()
    if self.autosave then
        self:save()
    end
end

--- @internal Assignment meta-method.
-- 
-- FIXME
-- 
function settings:__newindex(key,value)
    assert(type(key) == 'string')
    
    local klist = {}
    for _,k in ipairs(strsplit(key, '[.]')) do
        local s,n = string.match(k, '^([a-zA-Z_]%a*)[[]([1-9][0-9]*)[]]$')
        if s then
            tinsert(klist, s)
            tinsert(klist, tonumber(n))
        else
            tinsert(klist, k)
        end    
    end
    
    local acc
    local p = rawget(self, 'settings')
    for i,k in ipairs(klist) do
        if i == #klist then
            key = k
            break
        end
        
        -- processed keys (accumulated)
        if acc then
            if type(k) == 'number' then
                acc = acc .. '[' .. tostring(k) .. ']'
            else
                acc = acc .. '.' .. k
            end
        else
            acc = k
        end

        -- auto-create table
        if p[k] == nil then
            p[k] = {}
        end
        
        -- next 
        p = p[k]

        -- sanity check
        if type(p) ~= 'table' then
            error("Expected a table at '"..acc.."' but encountered a '"..type(p).."'.")
        end
    end
    
    -- assign
    if p[key] ~= nil and value ~= nil then
        if type(p[key]) ~= type(value) then
            error("Cannot assign a '"..type(value).."' to '"..acc.."."..key.."' which is of type '"..type(p).."'.")
        end
    end    

    if type(key) == 'number' then
        -- for number keys, uniquely insert into the table,
        for i,v in ipairs(p) do
            if v == value then
                return
            end
        end

        tinsert(p, value)
    else
        -- otherwise, simple assign by key
        p[key] = value
    end
end

--- @internal Table-index meta-method.
-- 
-- FIXME
-- 
function settings:__index(k)
    local member = rawget(self, k)
    local mt = getmetatable(self)
    
    if member ~= nil then
        return member
    elseif mt and mt[k] then
        return mt[k]
    else
        -- return setting
        assert(type(k) == 'string')
        local klist = strsplit(k, '[.]')
        
        local curkey
        local p = rawget(self, 'settings')
        for i,k in ipairs(klist) do
            p = p[k]
            
            -- processed keys
            if curkey then
                curkey = curkey .. '.' .. k
            else
                curkey = k
            end
            
            -- sanity check
            if i < #klist then
                if type(p) ~= 'table' then
                    error("Expected a table at '"..curkey.."' but encountered a '"..type(p).."'.")
                end
            end
        end
        return p
    end
end

--- Reset configuration.
-- 
-- 
function settings:reset(t)
    rawset(self, 'settings', tdup(t or {}))
end

--- Load configuration from file.
function settings:load()
    -- load chunk and run it
    local filename = rawget(self, 'filename')
    local settings = {}
    
    if not io.open(filename) then
        -- no such file exists...
        return
    end
    
    local chunk,message = loadfile(filename, "t", settings)
    if not chunk then
        error(message)
    end
    
    local status,message = pcall(chunk)
    if not status then
        return status,message
    end
    
    -- parse it
    local curkey
    local function assign(key, tbl)
        if key then
            if curkey then
                curkey = curkey .. '.' .. key
            else
                curkey = key
            end
        end
        
        for k,v in pairs(tbl) do
            local key = curkey
            if key then
                if type(k) == 'number' then
                    key = key .. '[' .. tostring(k) .. ']'
                else
                    key = key .. '.' .. k
                end
            else
                key = k
            end
        
            if type(v) == 'table' then
                assign(k, v)
            else
                self[key] = v
            end
        end
    end
    
    assign(nil, settings)
end

--- Explicitly save configuration to given file.
-- 
-- 
function settings:save()
    local filename = rawget(self, 'filename')
    local pathname = fs.filepath(filename);
    
    if not fs.exists(pathname) then
        local status,message = fs.mkdirp(pathname)
        if not status then
            error("Could not create configuration file directory: " .. message)
        end
    elseif not fs.isdir(pathname) then
        error("Configuration file: '"..pathname.." exists but is not a directory.")
    end

    local s = io.file(filename, 'w')
    table.dump(rawget(self, 'settings'), {stream = s, skip = true})
end

--[[ MODULE ]]
M.settings = settings
return M

