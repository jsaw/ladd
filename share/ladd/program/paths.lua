--[[
Copyright (C) 2018-2019 Research center caesar, Bonn, Germany
Author(s): 
    Jürgen "George" Sawinski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]


local M = require 'ladd.program._module_'

local sys = require 'ladd.sys'
local fs = require 'ladd.fs'

local tinsert = table.insert
local tunpack = table.unpack

local dirsep = package.config:sub(1,1)
local pathsep = package.config:sub(3,3)
local pattern = package.config:sub(5,5)

local UNIX = sys.UNIX
local WIN32 = sys.WIN32
local APPLE = sys.APPLE

math.randomseed(os.time())

-- @internal Program invocation name.
local invocation_name = string.match(arg[0], '.*'..dirsep..'(.*)')

-- @internal Program invocation directory.
local invocation_path = string.match(arg[0], '(.*)'..dirsep..'.*')

M.__vendor__ = nil
M.__package__ = invocation_name

--- Set vendor name.
-- 
-- FIXME
-- 
function M.set_vendor(vendor)
    M.__vendor__ = vendor
    
    if(UNIX and not APPLE and not WIN32) then
        M.__vendor__ = vendor:lower()
    end
end

--- Set package name.
-- 
-- FIXME
-- 
function M.set_package(package)
    M.__package__ = package
    
    if UNIX and not APPLE and not WIN32 then
        M.__package__ = package:lower()
    end
end

--- Get program name.
--
function M.invocation_name()
    return invocation_name
end

--- Get program invocation path.
-- 
function M.invocation_path()
    return invocation_path
end

--- Get program settings path.
-- 
function M.settings_path()
    local confdir = { sys.appdata() }
    
    tinsert(confdir, M.__vendor__)
    tinsert(confdir, M.__package__)
    
    return fs.fullfile(tunpack(confdir))
end

--- Get program temporary directory
-- 
function M.temporary_path() 
    local confdir = { sys.tmpdir() }
    
    tinsert(confdir, M.__vendor__)
    tinsert(confdir, M.__package__)
    
    if WIN32 and not UNIX then
        tinsert(confdir, 'Temp')
    elseif APPLE then
        tinsert(confdir, 'Temporary')
    end
    
    return fs.fullfile(tunpack(confdir))
end

--- Get program cache directory
-- 
function M.cache_path() 
    local confdir = { sys.cachedir() }
    
    tinsert(confdir, M.__vendor__)
    tinsert(confdir, M.__package__)
    
    if WIN32 and not UNIX then
        tinsert(confdir, 'Cache')
    elseif APPLE then
        tinsert(confdir, 'Cache')
    end
    
    return fs.fullfile(tunpack(confdir))
end

--[[ MODULE ]]
return M

