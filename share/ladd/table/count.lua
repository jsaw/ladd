--[[
Copyright (C) 2018-2019 Research center caesar, Bonn, Germany
Author(s): 
    Jürgen "George" Sawinski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local M = require 'ladd.table.module'

--- Count all elements in a table.
-- 
-- @note This might lead to unexpected results when the table has a meta-table,
--       and, especially if the __pairs meta-method is defined.
-- 
-- @param tbl A table.
local function tcount(tbl) 
    local cnt = 0
    
    for _,_ in pairs(tbl) do
        cnt = cnt + 1
    end
    
    return cnt
end

--- Check if a table is empty.
-- 
-- @note The behavior in combination with meta-tables might yield unexpected results.
-- 
-- @param tbl A table.
--
local function tempty(tbl)
    for _,_ in pairs(tbl) do
        return false
    end
    return true
end

--[[ MOUDLE ]]
M.count = tcount
M.empty = tempty
return M
