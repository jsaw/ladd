--[[
Copyright (C) 2018-2019 Research center caesar, Bonn, Germany
Author(s): 
    Jürgen "George" Sawinski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local M = require 'ladd.table.count'

local tcount = M.count
local tempty = M.empty

-- require/local
local io = require 'ladd.io'
local cls = require 'ladd.class'

local isclass = cls.isclass
local isobject = cls.isobject

local class_tags = cls.__reserved__

--- Dump a table to a stream.
-- 
-- @param tbl The table to dump.
-- @param options Options.
-- @internal @param __depth__ Table and indentation depth.
-- @internal @param __cyclic__ Internal table to avoid cyclic links.
-- 
-- FIXME describe options
-- 
local function tdump(tbl, options, __depth__, __cyclic__)
    -- default options
    options = options or {}
    options.stream = options.stream or ladd.io.stdout
    options.indent = options.indent or '    '
    
    -- default internals
    local s = options.stream
    local indent = options.indent

    __depth__ = __depth__ or 1

    -- skip initial {}
    if options.skip then
        __depth__ = __depth__ - 1
    end
    
    local comma = ','
    if __depth__ <= 0 then
        comma = ''
    end
    
    -- value format function
    local function output(v)
        if type(v) == 'table' then
            if tempty(v) == 0 then
                s:format("{}%s\n", comma)
            elseif isclass(v) then
                s:format("%s%s\n", v.__class__, comma)
            elseif isobject(v) then
                s:format("%s ", v.__class__)
                -- FIXME this will print a lot of fields we do not want 
                --       (esp. __super__, __members__, etc)
                --       maybe use v:dump(indent:rep(__depth__) ?
                tdump(v, {indent = indent, stream = s}, __depth__+1, __cyclic__)
            else
                if __cyclic__[v] then
                    s:format("{<cyclic>}%s\n", comma)
                else
                    tdump(v, {indent = indent, stream = s}, __depth__+1, __cyclic__)
                end
            end
        elseif type(v) == 'string' then
            s:format("%q%s\n", v, comma)
        else
            s:format("%s%s\n", tostring(v), comma)
        end
    end
    
    -- handle cyclic dependencies
    __cyclic__      = __cyclic__ or {}
    __cyclic__[tbl] = true 

    -- userdata
    if type(tbl) == 'userdata' then
        -- FIXME
        s:format("<userdata>\n")
        return
    end
    
    -- start table
    if not options.skip then
        s:format("{\n", indent:rep(__depth__-1))
    end
    
    -- output array (and remember 'ordered' keys)
    local ordered = {}
    for k,v in ipairs(tbl) do
        ordered[k] = true
        s:format("%s", indent:rep(__depth__))
        output(v)
    end

    -- output unordered set
    for k,v in pairs(tbl) do
        if not ordered[k] and class_tags[k] == nil then
            -- format key
            local key
            
            if type(k) == 'string' then
                if k:match("^[%a_][%a%d_]*$") then
                    key = k
                else
                    key = string.format('[%q]', k)
                end
            else
                key = string.format('[%s]', tostring(k))
            end
            
            s:format("%s%s = ", indent:rep(__depth__), key)
             
            -- output values
            output(v)
        end
    end

    if not options.skip then
        s:format("%s}\n", indent:rep(__depth__-1))
    end
end

--[[ MODULE ]]
M.dump = tdump

return M
