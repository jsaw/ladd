--[[
Copyright (C) 2018-2019 Research center caesar, Bonn, Germany
Author(s): 
    Jürgen "George" Sawinski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local M = require 'ladd.table.module'

--- Merge two tables.
-- @param T Destination table.
-- @param src Table to merge.
-- @param options Merging options.
-- 
function M.merge(T, src, options)
    -- default options
    options = options or {}
    options.recurse = options.recurse ~= nil and options.recurse or true

    -- merge
    for k,v in pairs(src) do
        if type(T[k]) == 'table' then
            if options.recurse then
                merge(T[k], v)
            else
                T[k] = v
            end
        else
            T[k] = v
        end
    end
    
    return T
end

--[[ MODULE ]]
M.merge = merge

return M
