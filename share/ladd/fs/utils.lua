--[[
Copyright (C) 2016-2019 Research center caesar, Bonn, Germany
Author(s): 
    Jürgen "George" Sawinski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local M = require 'ladd.libfs'

local dirsep = package.config:sub(1,1)
local tinsert = table.insert
local tremove = table.remove
local tunpack = table.unpack

local lfsdir = M.dir -- keep internal copy of lfs' dir()

--- Combine path elements.
-- 
-- This function, inspired by Matlab® concatenates path elements using
-- the system's (or rather Lua's, respectively) directory separator.  
-- 
-- @param ... Path elements
-- @return Path with system's directory separator.
-- 
-- @see M.dirsep
function M.fullfile(...)
    local arg = {...}
    local res = arg[1]
    
    for i = 2,#arg do
        res = res .. dirsep .. arg[i]
    end
    
    return res
end

--- Get path of a file.
-- 
function M.filepath(file)
    return file:match("^(.*)["..dirsep.."][^"..dirsep.."]*$")
end

--- Get file parts.
-- @param file The relative or absolute file name.
-- @returns path,base-name 
function M.fileparts(file)
    local fpath = file:match("^(.*)["..dirsep.."][^"..dirsep.."]*$") or '.'
    local fname = file:match("^.*["..dirsep.."]([^"..dirsep.."]*)$") or file

    return fpath,fname
end

--- Check if @a path exists.
-- 
function M.exists(path)
    return M.attributes(path) ~= nil
end

--- Check if @a path is a directory.
-- 
function M.isdir(path)
    local attr = M.attributes(path)
    return attr and attr.mode == 'directory'
end
--- Recursive directory iterator.
-- 
-- @see ladd.M.dir()
function M.rdir(path)
    local rec = { {path,lfsdir(path)} }
    
    return function()
    ::return_from_recursive::
        if #rec > 0 then
            local cwd,fn,u = tunpack(rec[#rec])
            local res = fn(u)
            
            while res == '.' or res == '..' do
                if #rec == 1 and res == '.' then
                    return path
                end

                res = fn(u)
            end

            if not res then
                tremove(rec)
                goto return_from_recursive
            end
            
            res = M.fullfile(cwd, res)
            
            if M.isdir(res) then
                tinsert(rec, {res, lfsdir(res)})
            end
            
            return res
        end
    end    
end

--- Standard directory seperator.
M.dirsep = dirsep

--- Standard search-path seperator.
M.pathsep = package.config:sub(3,3)

--- Standard Lua search-pattern.
M.searchpattern = package.config:sub(5,5)

--- Get module path.
-- 
-- @param modname The module name (e.g. 'ladd.util.fs').
-- @param searchpath The module search path (e.g. @c package.path)
-- 
function M.modulepath(modname, searchpath)
    -- find module in searchpath
    local fname
    if searchpath then
        fname = package.searchpath(modname, searchpath)
    else
        fname = package.searchpath(modname, package.path)
            or package.searchpath(modname, package.cpath)
    end

    -- get path        
    if fname then
        local pattern = dirsep .. modname:gsub('[.]', dirsep) 
        return fname
            :gsub(pattern.. dirsep .. 'init[.]lua$', '')
            :gsub(pattern.. '[.]lua$', '')
    end
end

--[[ MODULE ]]
return M

