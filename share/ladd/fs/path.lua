--[[
Copyright (C) 2016-2019 Research center caesar, Bonn, Germany
Author(s): 
    Jürgen "George" Sawinski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

--[[ @module ladd.fs ]]
local fs = require 'ladd.libfs'

local string = require 'string'

local dirsep = package.config:sub(1,1)
local tinsert = table.insert
local tunpack = table.unpack

local tpush = table.insert
local tpop = function(t)
    table.remove(t, #t)
end

local strsplit = string.split

--- Check if path is absolute.
-- 
local function isabsolute(fpath)
    return fpath:match("^(/.*)|([A-Za-z]:).*")
end

--- Convert to a canonical path.
-- 
-- FIXME
-- 
local function canonical(fpath)
    return fpath:gsub('[/\\]', '/')
end

--- Convert to absolute path.
-- 
-- FIXME
-- 
local function abspath(fpath)
    if not isabsolute(fpath) then
        return canonical(fpath):gsub('/', dirsep)
    end

    local res = { strsplit(fs.currentdir(), '[/\\]') }
    
    -- split elements
    local elem = strsplit(canonical(fpath), '/')
    assert(#elem > 0)
    
    -- combobulate    
    for _,p in ipairs(elem) do
        if p == '.' then
            -- ignore
        elseif p == '..' then
            tpop(res)
            if #res == 0 then
                tpush(res, '/') -- FIXME Windows?
            end
        else
            tpush(res, p)
        end
    end
    
    return fs.fullfile(tunpack(res))
end

--[[ MODULE ]]
fs.isabsolute = isabsolute
fs.canonical = canonical
fs.abspath = abspath
return fs
