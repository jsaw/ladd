# Essential Lua additions.

Ladd provides essential Lua additions:
- class implementation
- file system utilities
- table utilities
- extended IO capabilities
etc.

Full API documentation is available on [jsaw.bitbucket.io/ladd](jsaw.bitbucket.io/ladd).

## Notes

With the exception of ``class``, Ladd follows the style of returning a module table
(see [Modules Tutorial](http://lua-users.org/wiki/ModulesTutorial)) avoiding the
possiblity to override module functions, and, at the same time avoids spilling of 
the global table.



## Classes

Lua inheritly provides a mechanism for object-oriented programming through 
[meta-tables](https://www.lua.org/pil/13.html). The main language, however,
defines no explicit language constructs for implementing a ``class`` definition, 
and, therefore, a multitude of possible implementations exist. In all honesty,
here, another implementation is declared - which the author(s) use in a number
of semi-private projects.

**Usage**:  

A typical (global) class declaration, using *Ladd* is:

	class 'mynamespace.myclass' {
	    __inherit = { mynamespace.otherclass },
	    
	    member_variable = null,
	}
	
	funciton mynamespace.myclass:__init(...)
	    -- constructor code
	end
	
	funciton mynamespace.myclass:__gc(...)
	    -- destructor code
	end
	
The equivalent local declaration (within modules) is:

	local myclass = class '@mynamespace.myclass' {
	    __inherit = { mynamespace.otherclass },
	    
	    member_variable = null,
	}
	
	funciton myclass:__init(...)
	    -- constructor code
	end
	
	funciton myclass:__gc(...)
	    -- destructor code
	end

Refer to the [API documentation](jsaw.bitbucket.io/ladd/class) for further information.
