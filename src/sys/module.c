#include "module.h"

#include <lauxlib.h>

static int ladd_sys_name(lua_State *L) {
    lua_pushliteral(L, CMAKE_SYSTEM_NAME);
    return 1;
}

static int ladd_sys_version(lua_State *L) {
    lua_pushliteral(L, CMAKE_SYSTEM_VERSION);
    return 1;
}

static int ladd_sys_processor(lua_State *L) {
    lua_pushliteral(L, CMAKE_SYSTEM_PROCESSOR);
    return 1;
}

static const struct luaL_Reg syslib[] = {
        {"name", ladd_sys_name},
        {"version", ladd_sys_version},
        {"processor", ladd_sys_processor},
        {NULL, NULL},
};

#if LUA_VERSION_NUM >= 502
#  define new_lib(L, l) (luaL_newlib(L, l))
#else
#  define new_lib(L, l) (lua_newtable(L), luaL_register(L, NULL, l))
#endif

int luaopen_ladd_libsys(lua_State *L) {
    new_lib (L, syslib);

    lua_pushliteral(L, CMAKE_HOST_SYSTEM);
    lua_setfield(L, -2, "SYSTEM");

    lua_pushboolean(L, CMAKE_HOST_WIN32);
    lua_setfield(L, -2, "WIN32");

    lua_pushboolean(L, CMAKE_HOST_UNIX);
    lua_setfield(L, -2, "UNIX");

    lua_pushboolean(L, CMAKE_HOST_APPLE);
    lua_setfield(L, -2, "APPLE");

    return 1;
}
