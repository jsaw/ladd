/*
 */
#ifndef SRC_FS_MODULE_H_
#define SRC_FS_MODULE_H_

#include <lua.h>

#ifndef LUA_API_IMPORT
#define LUA_API_IMPORT LUA_API
#endif

#if defined(LADD_BUILD)
#define LADD_API LUA_API_EXPORT
#else
#define LADD_API LUA_API_IMPORT
#endif

LADD_API int luaopen_ladd_libsys(lua_State *L);

#ifdef __cplusplus
}
#endif

#endif /* SRC_FS_MODULE_H_ */
