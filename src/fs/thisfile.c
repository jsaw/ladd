/* 'thisfile' is taken from giralua (originally torch/qtlua)
 *
 *
 * Copyright (c) 2018-2019 Research center caesar (Juergen "George" Sawinski)
 * Copyright (c) 2011-2014 Idiap Research Institute (Ronan Collobert)
 * Copyright (c) 2012-2014 Deepmind Technologies (Koray Kavukcuoglu)
 * Copyright (c) 2011-2012 NEC Laboratories America (Koray Kavukcuoglu)
 * Copyright (c) 2011-2013 NYU (Clement Farabet)
 * Copyright (c) 2006-2010 NEC Laboratories America (Ronan Collobert, Leon Bottou, Iain Melvin, Jason Weston)
 * Copyright (c) 2006      Idiap Research Institute (Samy Bengio)
 * Copyright (c) 2001-2004 Idiap Research Institute (Ronan Collobert, Samy Bengio, Johnny Mariethoz)
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the names of Deepmind Technologies, NYU, NEC Laboratories America,
 *    IDIAP Research Institute, and, Resaearch center caesar nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "module.h"

#include <string.h>

#include <lauxlib.h>
#include <lualib.h>

int luaQ_thisfile(lua_State *L) {
    lua_Debug ar;

    if (!lua_getstack(L, (int)luaL_optinteger(L, 2, 1), &ar))
        return luaL_argerror(L, 2, "stack level out of range");

    if (!lua_getinfo(L, "S", &ar))
      return luaL_argerror(L, 1, "getinfo failed"); /* FIXME error message */

    const char *fname = luaL_optstring(L, 1, NULL);
    const char *currentfile = ar.source+1;

    const char *sep = strrchr(currentfile, LUA_DIRSEP[0]);
    if (sep) {
        const char *path = lua_pushlstring(L, currentfile, sep-currentfile);
        const char *file = lua_pushstring(L, sep+1);
        if (fname)
            lua_pushfstring(L, "%s%s%s", path, LUA_DIRSEP, fname);
        else
            lua_pushstring(L, currentfile);
    }
    else {
        lua_pushstring(L, ".");
        lua_pushstring(L, currentfile);
        if (fname)
            lua_pushstring(L, fname);
        else
            lua_pushstring(L, currentfile);
    }

    lua_insert(L, -3);

    return 3;
}

