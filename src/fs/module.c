#include "module.h"

#include <lauxlib.h>

static const struct luaL_Reg fslib[] = {
        /* LuaFileSystem */
        {"attributes",      file_info},
        {"chdir",           change_dir},
        {"currentdir",      get_dir},
        {"dir",             dir_iter_factory},
        {"link",            make_link},
        {"lock",            file_lock},
        {"mkdir",           make_dir},
        {"rmdir",           remove_dir},
        {"symlinkattributes", link_info},
        {"setmode",         lfs_f_setmode},
        {"touch",           file_utime},
        {"unlock",          file_unlock},
        {"lock_dir",        lfs_lock_dir},
        /* torch/qtlua */
        {"thisfile",        luaQ_thisfile},
        {NULL, NULL},
};

#if LUA_VERSION_NUM >= 502
#  define new_lib(L, l) (luaL_newlib(L, l))
#else
#  define new_lib(L, l) (lua_newtable(L), luaL_register(L, NULL, l))
#endif

int luaopen_ladd_libfs (lua_State *L) {
    dir_create_meta (L);
    lock_create_meta (L);
    new_lib (L, fslib);
    return 1;
}
