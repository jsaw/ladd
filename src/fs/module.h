/*
 */
#ifndef SRC_FS_MODULE_H_
#define SRC_FS_MODULE_H_

#include <lua.h>

#ifndef LUA_API_IMPORT
#define LUA_API_IMPORT LUA_API
#endif

#if defined(LADD_BUILD)
#define LADD_API LUA_API_EXPORT
#else
#define LADD_API LUA_API_IMPORT
#endif

#define chdir_error strerror(errno)

#ifdef _WIN32
  #define chdir(p) (_chdir(p))
  #define getcwd(d, s) (_getcwd(d, s))
  #define rmdir(p) (_rmdir(p))
  #ifndef fileno
    #define fileno(f) (_fileno(f))
  #endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

/** @fn ladd.fs.attributes
 ** FIXME
 */
LUAI_FUNC int file_info(lua_State *L);

//{"chdir",           change_dir},
LUAI_FUNC int change_dir(lua_State *L);
//    {"currentdir",      get_dir},
LUAI_FUNC int get_dir(lua_State *L);
//    {"dir",             dir_iter_factory},
LUAI_FUNC int dir_iter_factory(lua_State *L);
//    {"link",            make_link},
LUAI_FUNC int make_link(lua_State *L);
//    {"lock",            file_lock},
LUAI_FUNC int file_lock(lua_State *L);
//    {"mkdir",           make_dir},
LUAI_FUNC int make_dir(lua_State *L);
//    {"rmdir",           remove_dir},
LUAI_FUNC int remove_dir(lua_State *L);
//    {"symlinkattributes", link_info},
LUAI_FUNC int link_info(lua_State *L);
//    {"setmode",         lfs_f_setmode},
LUAI_FUNC int lfs_f_setmode(lua_State *L);
//    {"touch",           file_utime},
LUAI_FUNC int file_utime(lua_State *L);
//    {"unlock",          file_unlock},
LUAI_FUNC int file_unlock(lua_State *L);
//    {"lock_dir",        lfs_lock_dir},
LUAI_FUNC int lfs_lock_dir(lua_State *L);
//    /* torch/qtlua */
//    {"thisfile",        luaQ_thisfile},
LUAI_FUNC int luaQ_thisfile(lua_State *L);

LUAI_FUNC int dir_create_meta (lua_State *L);
LUAI_FUNC int lock_create_meta (lua_State *L);

LADD_API int luaopen_ladd_libfs(lua_State *L);

#ifdef __cplusplus
}
#endif

#endif /* SRC_FS_MODULE_H_ */
