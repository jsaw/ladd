## Default script output directory.
# 
if(NOT DEFINED SOURCE_OUTPUT_PATH)
	set(SOURCE_OUTPUT_PATH ${PROJECT_BINARY_DIR})
endif()

## Parse Lua module name into a file-system path.
#
#     lua_module_name(<lua-module-name> <output-variable>)
# 
# Converts a Lua module name (such as 'ladd.class') into a 
# file-system path (here, 'ladd/class').
#
function(lua_module_name modulename outvar)
	string(REGEX REPLACE "[.]+" "/" ${outvar} "${modulename}")
	set(${outvar} ${${outvar}} PARENT_SCOPE) 
endfunction()

## Find Lua script files in a directory
#
#     lua_script_find(<module-path> <output-list-variable>
#         [DIRECTORY <working-directory>]
#     )
# 
# Finds all files for a base module path (which is included in the 
# result list) relative to the working directory (which, if omitted,
# is the current source directory).
#
function(lua_script_find modulepath outvar)
	cmake_parse_arguments(arg "" "DIRECTORY" "" ${ARGN})
	
	if(NOT DEFINED arg_DIRECTORY)
		set(arg_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
	endif()

	file(GLOB_RECURSE scriptlist "${arg_DIRECTORY}/${modulepath}/*")
	
	set(result)
	foreach(item ${scriptlist})
		file(RELATIVE_PATH item "${arg_DIRECTORY}" "${item}")

		list(APPEND result "${item}")
	endforeach()

	set(${outvar} ${result} PARENT_SCOPE) 
endfunction()

## Add Lua script target.
#
#     lua_add_script(<module-name> [DIRECTORY]
#     )
# 
# Creates a installation target. 
#
# Additionally, to facilitate simple debugging, this macro creates a target 
# which copies all script modules into the ``SOURCE_OUTPUT_PATH``.
# 
# @see lua_add_module
#
macro(lua_add_script modulename)
	# parse arguments
	cmake_parse_arguments(__ladd_script "DIRECTORY" "" "" ${ARGN})
	
	lua_module_name(${modulename} __ladd_script_PATH)

#	if(NOT TARGET ladd-script)
#		add_custom_target(ladd-script ALL)
#    endif()
    
    # create targets
    if(__ladd_script_DIRECTORY)
	    if(UNIX)
		    lua_script_find(${__ladd_script_PATH} __ladd_script_FILES)
			foreach(item ${__ladd_script_FILES})
				string(REGEX REPLACE "[.]lua$" "" modname "${item}")
				string(REGEX REPLACE "[/\\]+" "." modname "${modname}")
				get_filename_component(dir "${item}" DIRECTORY)

#				if(NOT TARGET ${modname}-dir)
#					add_custom_target(${modname}-dir
#						DEPENDS ${modname}-dir ${CMAKE_CURRENT_SOURCE_DIR}/${dir}
#					    COMMAND ${CMAKE_COMMAND} -E make_directory ${SCRIPT_OUTPUT_PATH}/${dir})
#			    endif()
#			    
#				add_custom_target(${modname}
#					DEPENDS ${modname}-dir ${CMAKE_CURRENT_SOURCE_DIR}/${item}
#				    COMMAND ${CMAKE_COMMAND} -E remove ${SCRIPT_OUTPUT_PATH}/${item}
#				    COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_CURRENT_SOURCE_DIR}/${item} ${SCRIPT_OUTPUT_PATH}/${item})
#			    add_dependencies(ladd-script ${modname})
			endforeach()		    
	    else()
#		    add_custom_target(${modulename}
#			    DEPENDS ${__ladd_script_FILES}
#			    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/${__ladd_script_PATH} ${SCRIPT_OUTPUT_PATH}/${__ladd_script_PATH})
#		    add_dependencies(ladd-script ${modulename})
	    endif()

		install(DIRECTORY ${__ladd_script_PATH}
			DESTINATION ${LUA_PATH_DIR}
			COMPONENT "runtime")
    else()
#	    add_custom_target(${modulename}
#		    DEPENDS ${__ladd_script_PATH}.lua
#		    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/${__ladd_script_PATH}.lua ${SCRIPT_OUTPUT_PATH}/${__ladd_script_PATH}.lua)
#	    add_dependencies(ladd-script ${modulename})

		install(FILES ${__ladd_script_PATH}.lua
			DESTINATION ${LUA_PATH_DIR}/${__ladd_script_PATH}.lua 
			COMPONENT "runtime")
    endif()
endmacro()

## Add Lua module target.
#
#     lua_add_module(<module-name>
#         <source-files>
#         [LINK <link-libraries>]
#         [DEFINES <compiler-definitions>]
#         [FEATURES <compiler-features>]
#         [OPTIONS <compiler-options>]
#         [INCLUDE_DIRECTORIES <include-directories>]
#     )
# 
# Create build and installation target for a Lua module. 
#
macro(lua_add_module modulename)
	# parse arguments
	cmake_parse_arguments(__ladd_module "" "" "LINK;DEFINES;FEATURES;OPTIONS;INCLUDE_DIRECTORIES" ${ARGN})
	
	string(REGEX REPLACE "^.*[.]" "" __ladd_module_NAME "${modulename}")
	string(REGEX REPLACE "[.][^.]+$" "" __ladd_module_PATH "${modulename}")
	lua_module_name(${__ladd_module_PATH} __ladd_module_PATH)

#	# create output directory
#	if(NOT TARGET ${modname}-dir)
#		add_custom_target(${modulename}-dir
#			DEPENDS ${modulename}-dir
#		    COMMAND ${CMAKE_COMMAND} -E make_directory ${LIBRARY_OUTPUT_PATH}/${__ladd_module_PATH})
#    endif()
	
	# add target with properties
	add_library(${__ladd_module_NAME} MODULE ${__ladd_module_UNPARSED_ARGUMENTS})
#	add_dependencies(${__ladd_module_NAME} ${modulename}-dir)
	set_target_properties(${__ladd_module_NAME} PROPERTIES
		PREFIX ""
		LIBRARY_OUTPUT_DIRECTORY ${LIBRARY_OUTPUT_PATH}/${__ladd_module_PATH})
		
	if(DEFINED __ladd_module_LINK)
		target_link_libraries(${__ladd_module_NAME} ${__ladd_module_LINK})
	endif()

	if(DEFINED __ladd_module_DEFINES)
		target_compile_definitions(${__ladd_module_NAME} ${__ladd_module_DEFINES})
	endif()

	if(DEFINED __ladd_module_FEATURES)
		target_compile_features(${__ladd_module_NAME} ${__ladd_module_FEATURES})
	endif()

	if(DEFINED __ladd_module_OPTIONS)
		target_compile_options(${__ladd_module_NAME} ${__ladd_module_OPTIONS})
	endif()

	if(DEFINED __ladd_module_INCLUDE_DIRECTORIES)
		target_include_directories(${__ladd_module_NAME} ${__ladd_module_INCLUDE_DIRECTORIES})
	endif()
	
	# install
	install(TARGETS ${__ladd_module_NAME}
		RUNTIME DESTINATION ${LUA_CPATH_DIR}/${__ladd_module_PATH} COMPONENT "runtime"
		LIBRARY DESTINATION ${LUA_CPATH_DIR}/${__ladd_module_PATH} COMPONENT "runtime"
	)
endmacro()

## Add 'Lua nature' to project files.
# 
function(lua_add_natures)
	# Eclipse
	get_property(eclipse_extra_natures GLOBAL 
		PROPERTY ECLIPSE_EXTRA_NATURES)
	list(APPEND eclipse_extra_natures "org.eclipse.ldt.nature")
	list(REMOVE_DUPLICATES eclipse_extra_natures)
	set_property(GLOBAL 
		PROPERTY ECLIPSE_EXTRA_NATURES "${eclipse_extra_natures}")
endfunction()

lua_add_natures()
